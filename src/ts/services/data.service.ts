import { INVESTMENT_API } from '../constants/api';

export type TCurrency = 'GBP' | 'EUR' | 'USD' | 'etc';

export type TCurrencyData = [number, TCurrency];

export interface IInvestmentData {
  current: TCurrencyData;
  target: TCurrencyData;
  percentage: number;
}

export interface IImageData {
  id: string;
  type: 'logo' | 'coverImage';
  src: string;
}

export interface IImageDataByType {
  logo?: IImageData;
  coverImage?: IImageData;
}

export interface ICompanyOverview {
  id: string;
  name: string;
  description: string;
  investment: IInvestmentData;
  equity: string;
  images: IImageData[];
  updated_at: string;
  expires_at: string;
  // custom properties
  imagesByType?: IImageDataByType;
}

export default class DataService {
  // This method handles faking the various parts of the request. Pagination & search
  // TODO: Add a sort arg
  // TODO: Pass the arguments on to the API once using an actual endpoint
  static getInvestments(
    resultsPerPage: number = 6,
    page: number = 1,
    searchTerm: string = '',
  ): Promise<ICompanyOverview[]> {
    return (
      fetch(INVESTMENT_API)
        .then(response => response.json())
        .then(data => data as ICompanyOverview[])
        // Simple filter to mimic the API
        .then(data => data.filter(result => result.name.toLocaleLowerCase().indexOf(searchTerm.toLowerCase()) > -1))
        // Mimic some pagination
        .then(data => data.splice((page - 1) * resultsPerPage, resultsPerPage))
        // Get the images from their array into a more usable object
        .then(data =>
          data.map(result => {
            result.imagesByType = result.images.reduce((obj: IImageDataByType, image) => {
              obj[image.type] = image;
              return obj;
            }, {});
            return result;
          }),
        )
    );
  }
}
