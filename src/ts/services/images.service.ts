// Helper method for loading an image, and resolving a promise once loaded
export const imageLoader = (src: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    const image = new Image();
    image.onload = () => {
      resolve();
    };

    image.onerror = () => {
      reject();
    };
    image.src = src;
  });
};
