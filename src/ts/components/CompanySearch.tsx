import * as React from 'react';

import { Company } from './Company';
import { Search } from './Search';
import data, { ICompanyOverview } from '../services/data.service';

export interface ICompanySearchState {
  searchResults: ICompanyOverview[];
  currentPage: number;
  resultsPerPage: number;
  loading: boolean;
  searchTerm: string;
}

export interface ICompanySearchProps {}

export class CompanySearch extends React.Component<ICompanySearchProps, {}> {
  state: ICompanySearchState;

  constructor(props: ICompanySearchProps) {
    super(props);

    this.state = {
      searchResults: [],
      currentPage: 0,
      resultsPerPage: 6,
      loading: false,
      searchTerm: '',
    };

    this.getData = this.getData.bind(this);
    this.searchValueChanged = this.searchValueChanged.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  private getData(resetPage?: boolean) {
    this.setState({ loading: true, ...(resetPage && { searchResults: [], currentPage: 0 }) }, () => {
      const pageToGet = this.state.currentPage + 1;

      data.getInvestments(this.state.resultsPerPage, pageToGet, this.state.searchTerm).then(data => {
        const searchResults = this.state.searchResults.concat(data);
        this.setState({ searchResults, currentPage: pageToGet, loading: false });
      });
    });
  }

  private searchValueChanged(newValue: string): void {
    console.log('wham', newValue);
    this.setState({ searchTerm: newValue }, () => {
      this.getData(true);
    });
  }

  render() {
    // TODO: swap in an actual loader
    const loader = this.state.loading ? <div className="text-center">Loading...</div> : null;

    const loadMoreButton = !this.state.loading ? (
      <button type="button" onClick={() => this.getData()} className="button button--load-more">
        Load More
      </button>
    ) : null;

    return (
      <div>
        <div className="search">
          <Search searchValueChanged={this.searchValueChanged} />
        </div>
        <div className="panel-container">
          {this.state.searchResults.map((item: any) => {
            return (
              <Company
                name={item.name}
                description={item.description}
                images={item.imagesByType}
                investment={item.investment}
                equity={item.equity}
              />
            );
          })}
        </div>

        <div className="text-center">
          {loader}
          {loadMoreButton}
        </div>
      </div>
    );
  }
}
