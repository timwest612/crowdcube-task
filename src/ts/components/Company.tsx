import * as React from 'react';
import { imageLoader } from '../services/images.service';
import { IImageDataByType, IInvestmentData } from '../services/data.service';
import { Investment } from './Investment';

export interface ICompanyProps {
  name: string;
  description: string;
  images: IImageDataByType;
  investment: IInvestmentData;
  equity: string;
}

export interface ICompanyState {
  logoLoaded: boolean;
  coverImageLoaded: boolean;
}

export class Company extends React.Component<ICompanyProps, {}> {
  state: ICompanyState;

  constructor(props: ICompanyProps) {
    super(props);

    this.state = {
      logoLoaded: false,
      coverImageLoaded: false,
    };
  }

  componentDidMount() {
    imageLoader(this.props.images.coverImage.src).then(() => {
      this.setState({ coverImageLoaded: true });
    });

    imageLoader(this.props.images.logo.src).then(() => {
      this.setState({ logoLoaded: true });
    });
  }

  // TODO: Swap for proper routing with React Router
  private companyUrl(name: string): string {
    return encodeURIComponent(name.toLowerCase());
  }

  render() {
    const logo = this.state.logoLoaded ? (
      <img src={this.props.images.logo.src} alt={`Logo for ${this.props.name}`} />
    ) : null;

    const coverImage = this.state.coverImageLoaded ? (
      <div
        className="company-banner__image"
        style={{ backgroundImage: `url("${this.props.images.coverImage.src}")` }}
      />
    ) : null;

    // TODO: Add favourite & days remaining
    return (
      <a href={`/company/${this.companyUrl(this.props.name)}`} className="panel panel__company company">
        <div className="company__banner company-banner">
          {coverImage}
          <div className="company-banner__logo">{logo}</div>
          <div className="company-banner__time-remaining" />
        </div>
        <div className="company__body">
          <h2 className="company__title">{this.props.name}</h2>
          <p className="company__description">{this.props.description}</p>

          <Investment investment={this.props.investment} equity={this.props.equity} />
        </div>
      </a>
    );
  }
}
