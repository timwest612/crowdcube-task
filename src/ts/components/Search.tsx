import * as React from 'react';

export interface ISearchProps {
  searchValueChanged: Function;
}

export interface ISearchState {
  value: string;
}

export class Search extends React.Component<ISearchProps, {}> {
  state: ISearchState;

  constructor(props: ISearchProps) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  private handleChange(event: React.FormEvent<HTMLInputElement>) {
    this.setState({ value: (event.target as HTMLInputElement).value });
  }

  private handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.props.searchValueChanged(this.state.value);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Search..." onChange={this.handleChange} />
      </form>
    );
  }
}
