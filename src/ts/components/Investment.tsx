import * as React from 'react';
import { IInvestmentData, TCurrencyData } from '../services/data.service';

export interface IInvestmentProps {
  investment: IInvestmentData;
  equity: string;
}

export class Investment extends React.Component<IInvestmentProps, {}> {
  private barPercentage(percentageFunded: number): number {
    // Keep the graph looking positive
    if (percentageFunded < 10) {
      return 10;
    }
    // Set to 100% to keep the CSS animation sensible
    if (percentageFunded > 100) {
      return 100;
    }
    return percentageFunded;
  }

  // TODO: Move to a helpers file. Or swap for a lib.
  private currencyFormatter([value, currency]: TCurrencyData): string {
    // TODO: Fix type
    const symbolMap: any = {
      GBP: '£',
      EUR: '€',
      USD: '$',
    };
    const currencyKnown: boolean = !!symbolMap[currency];

    // TODO: Insert commas where appropriate
    const valueString = `${value}`;
    return `${currencyKnown ? symbolMap[currency] : ''}${valueString}${currencyKnown ? '' : currency}`;
  }

  // TODO: Add aria labelling. Especially to the chart.
  render() {
    return (
      <div className="investment">
        <div className="investment__target">{this.currencyFormatter(this.props.investment.target)} Target</div>
        <div
          className={`investment__chart investment-chart ${
            this.props.investment.percentage >= 100 ? 'investment-chart--funded' : ''
          }`}
        >
          <div
            className="investment-chart__bar"
            style={{ width: `${this.barPercentage(this.props.investment.percentage)}%` }}
          />
          <div className="investment-chart__label">{this.props.investment.percentage}%</div>
        </div>
        <dl className="investment__info-container">
          <div className="investment__info">
            <dt>Raised</dt>
            <dd>{this.currencyFormatter(this.props.investment.current)}</dd>
          </div>
          <div className="investment__info">
            <dt>Equity</dt>
            <dd>{this.props.equity}</dd>
          </div>
        </dl>
      </div>
    );
  }
}
