import * as React from 'react';

import { CompanySearch } from './components/CompanySearch';

export class App extends React.Component<{}, {}> {
  render() {
    return (
      <div>
        <CompanySearch />
      </div>
    );
  }
}
