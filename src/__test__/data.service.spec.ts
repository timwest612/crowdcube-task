import DataService, { ICompanyOverview } from '../ts/services/data.service';
import { INVESTMENT_API } from '../ts/constants/api';

let mockData: ICompanyOverview[] = [];

describe('DataService', () => {
  beforeEach(function() {
    mockData = [
      {
        id: 'bjjPXb',
        name: 'BurningNight Group',
        description:
          'Launched in 2010 and now in six of the UK’s biggest cities, BurningNight Group’s concept bars, Bierkeller Entertainment Complexes, offer distinct, multi-venue experiences. With sales now topping £19.5m & profits of £595k for 2017, they are now focused on the roll-out of their new brand, Sportskeller',
        investment: {
          current: [16578000, 'GBP'],
          target: [75000000, 'GBP'],
          percentage: 22,
        },
        equity: '3.61',
        images: [
          {
            id: 'b3aDel',
            type: 'logo',
            src: 'b3aDel-logo-src',
          },
          {
            id: 'qaNoEl',
            type: 'coverImage',
            src: 'qaNoEl-coverImage-src',
          },
        ],
        updated_at: '2017-11-08 00:17:47',
        expires_at: '2017-12-02 23:59:59',
      },
      {
        id: 'qByr6b',
        name: 'TeachPitch',
        description:
          'Helping schools organise and manage the best of online learning, TeachPitch has gained strong traction with educational organisations. Since its first Crowdcube raise, TeachPitch has grown its user base and expanded in China through B2B partnerships, enabling access to hundreds of more schools.',
        investment: {
          current: [29570000, 'GBP'],
          target: [40000000, 'GBP'],
          percentage: 73,
        },
        equity: '8.51',
        images: [
          {
            id: 'qnnpjq',
            type: 'logo',
            src: 'qnnpjq-logo-src',
          },
          {
            id: 'gb3veZ',
            type: 'coverImage',
            src: 'gb3veZ-coverImage-src',
          },
        ],
        updated_at: '2017-10-26 08:53:56',
        expires_at: '2017-11-30 23:59:59',
      },
    ];

    // Not a nice polyfill. But time limits...
    // @ts-ignore
    global.fetch = jest.fn().mockImplementation(() => {
      return Promise.resolve({
        ok: true,
        json: function() {
          return mockData;
        },
      });
    });
  });

  describe('getInvestments', () => {
    it('should call the investments API', () => {
      DataService.getInvestments();
      expect(window.fetch).toHaveBeenCalledWith(INVESTMENT_API);
    });

    it('should add a restructured images object to the response', done => {
      DataService.getInvestments().then(result => {
        expect(result[0].imagesByType).toEqual({
          logo: {
            id: 'b3aDel',
            type: 'logo',
            src: 'b3aDel-logo-src',
          },
          coverImage: {
            id: 'qaNoEl',
            type: 'coverImage',
            src: 'qaNoEl-coverImage-src',
          },
        });
        expect(result[1].imagesByType).toEqual({
          logo: {
            id: 'qnnpjq',
            type: 'logo',
            src: 'qnnpjq-logo-src',
          },
          coverImage: {
            id: 'gb3veZ',
            type: 'coverImage',
            src: 'gb3veZ-coverImage-src',
          },
        });
        done();
      });
    });
  });
});
