import { imageLoader } from '../ts/services/images.service';

describe('imageLoader', () => {
  it('should return a promise', () => {
    const result = imageLoader('testsrc');
    expect(result).toEqual(jasmine.any(Promise));
  });
});
